import Fastify from 'fastify';
import FastifyHealthCheck from 'fastify-healthcheck';

// Require the framework and instantiate it
const fastify = Fastify({ logger: true });

fastify.register(FastifyHealthCheck);

// Declare a route
fastify.get('/', async (req) => {
  // GET /users?filters={"riderId":"1111"}
  const { query } = req;

  let rawFilters = '';

  if ('filters' in query) {
    rawFilters = query.filters;
  }

  console.log(query);
  return { hello: 'world' };
});

// Run the server!
const port = process.env.PORT ?? 8080;

const start = async () => {
  try {
    await fastify.listen(port, '0.0.0.0');
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
