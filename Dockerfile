FROM node:14-alpine AS builder

# couchbase sdk requirements
RUN apk update && apk add yarn curl bash python g++ make && rm -rf /var/cache/apk/*

# install node-prune (https://github.com/tj/node-prune)
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

WORKDIR /usr/src/app

COPY package.json ./

# install dependencies
RUN yarn --frozen-lockfile

COPY . .

# lint & test
# RUN yarn lint & yarn test

# build application
RUN yarn build

# remove development dependencies
RUN npm prune --production

# run node prune
RUN /usr/local/bin/node-prune

FROM alpine:3 AS server

LABEL version="0.0.1"
LABEL description="Example Fastify (Node.js) Docker Image"
LABEL maintainer="Matias Nahuel Améndola <soporte.esolutions@gmail.com>"

RUN apk add nodejs=14.17.1-r0 --no-cache

WORKDIR /usr/src/app

# copy from build image
COPY --from=builder /usr/src/app/dist ./
COPY --from=builder /usr/src/app/node_modules ./node_modules

EXPOSE 8080

HEALTHCHECK --interval=30s --timeout=10s --start-period=5s CMD node ./index.js

CMD [ "node", "./index.js" ]
