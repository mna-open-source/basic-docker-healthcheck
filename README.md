# Docker + Fastify + health check plugin

```shell
docker build . -t basic-docker-healthcheck:latest --no-cache
docker rmi -f $(docker images -f "dangling=true" -q)
docker run -id --rm --name basic-docker-healthcheck -p 8080:8080 basic-docker-healthcheck:latest
```

## Stop all containers

```shell
docker stop $(docker ps -a -q)
docker system prune -a
```

## References

- [Lightweight and Performance Dockerfile for Node.js](https://itnext.io/lightweight-and-performance-dockerfile-for-node-js-ec9eed3c5aef)
- [How We Reduce Node Docker Image Size In 3 Steps](https://medium.com/trendyol-tech/how-we-reduce-node-docker-image-size-in-3-steps-ff2762b51d5a)

## License

[MIT](./LICENSE)
